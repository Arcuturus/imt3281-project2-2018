package no.ntnu.imt3281.ludo.logic;

/**
 * Custom exception for handling not having enough players
 *
 *  Throws exception when there is not enough ACTUAL players (not null)
 *  to start a game.
 */
public class NotEnoughPlayersException extends RuntimeException {
	private int badNumber;

    public NotEnoughPlayersException(int noOfPlayers) {
        super("Not Enough Players Exception");
        badNumber = noOfPlayers;
    }
}
