package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;

/**
 * Event that handles the logic related to the Dice
 */
public class DiceEvent extends EventObject implements AutoCloseable {
	
	private int player, diceFace;

    /**
     * Initialize dice event
     * @param ludo ludo object
     * @param player which player
     * @param face the value of the face
     */
	public DiceEvent(Ludo ludo, int player, int face) {
		super(ludo);
		this.player = player;
		this.diceFace = face;
	}

	/* Setter and getter for player */
	public void setPlayer(int player) { this.player = player; }
	public int getPlayer() { return player; }
	
	@Override
	public boolean equals(Object obj) {
		try(DiceEvent dEvent = (DiceEvent) obj) {
			return (player == dEvent.player &&
					diceFace == dEvent.diceFace &&
					source == dEvent.source);
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
