package no.ntnu.imt3281.ludo.logic;
/**
 * Custom exception
 *
 * This exxeption is thrown if there is an attempt at adding a fifth player
 * to the ludo object
 */
public class NoRoomForMorePlayersException extends RuntimeException {
	
	public NoRoomForMorePlayersException() {
		super("NoRoomForMorePlayersException");
	}
}
