package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;

/**
 * Event that handles the logic related to the Player
 */
public class PlayerEvent extends EventObject implements AutoCloseable {
	/* Player status */
	public static int PLAYING = 0, WAITING = 1, LEFTGAME = 2, WON = 3;
	/* The current active player */
	public int currentActivePlayer;
	/* Current game state */
	public int gameState;

	/**
	 * Initialize a player event
	 * @param obj The player event object
	 * @param player1 Player 1
	 * @param player2 Player 2
	 */
	public PlayerEvent(Object obj, int player1, int player2) {
		super(obj);
		currentActivePlayer = player1;
		gameState = player2;
	}

	/**
	 *
	 * @param obj The player event object
	 * @return
	 */
	@Override
	public boolean equals(Object obj) {
		try(PlayerEvent pEvent = (PlayerEvent) obj) {
			return (currentActivePlayer == pEvent.currentActivePlayer &&
					gameState == pEvent.gameState &&
					source == pEvent.source);
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
			return false;
		}
	}

	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
