/**
 * The Ludo class controls all the logic related to a game
 */

package no.ntnu.imt3281.ludo.logic;

import java.util.ArrayList;
import java.util.Random;

public class Ludo {
    /* Set correct value to player pieces */
    public static int RED = 0, BLUE = 1, YELLOW = 2, GREEN = 3;
    /* The players. */
    private ArrayList<String> players;
    /* The currently active player */
    private int currentActivePlayer;
    /* The dice */
    private int dice;
    /*  Simulating dice throw */
    private Random random;
    /* Current game status */
    private String gameStatus;
    /* Player pieces relative to board (and other conversion logic) */
    private int[][] pieces;
    private int[][] userGridToPlayerGrid;
    /* The different event listeners */
    private ArrayList<DiceListener> dls;
    private ArrayList<PieceListener> pils;
    private ArrayList<PlayerListener> plls;
    /* If a player has one, set this to the winning player */
    private int winner;
    /* Number of throws of the current player */
    private int numOfThrows = 0;
    /* Used for logic in the throwDice function */
    private boolean nextPlayer = true;

    /**
     * Ludo contructor no players given
     */
    public Ludo() { createGame(); }

    /**
     * The de facto Ludo constructor. Initialises a ludo game with player names given
     * @param args player names to be added
     * @throws NotEnoughPlayersException thrown if there are less than two players in the game
     */
    public Ludo(String...args) throws NotEnoughPlayersException {
        boolean notEnoughPlayers = true;
        int numOfPlayersAdded = 0;
        createGame();
        /* Loop thorugh  */
        for(String name : args) {
            /* If name is null, skip adding player */
            if(name != null) {
                addPlayer(name);
                numOfPlayersAdded++;
            }
        }
        /* As long as there has been added more than 2 players */
        if(numOfPlayersAdded >= 2) notEnoughPlayers = false;
        /* Not enough players to start the game, throw exception */
        if (notEnoughPlayers) {
            throw new NotEnoughPlayersException(numOfPlayersAdded);
        } else {
            /* Set player pieces to start positions */
            pieces = new int[4][4];
            for (int i = 0; i < numOfPlayersAdded; i++) {
                for (int j = 0; j < 4; j++) {
                    pieces[i][j] = 0;
                }
            }
            gameStatus = "Initiated";
        }
    }

    /**
     * Initialize the game:
     */
    private void createGame() {
        userGridToPlayerGrid = new int[91][4];
        for (int i = 0; i < 16; i++) {
            userGridToPlayerGrid[i][0] = (int) Math.floor(i / 4);
        }
        for (int j = 0; j < 4; j++) {
            for (int i = 16; i < 91; i++) {
                userGridToPlayerGrid[i][j] = -1;
            }
        }
        /* Initialize random to be used as dice face */
        random = new Random();
        /* Initialize player list */
        players = new ArrayList<>();
        /* Initialize listener lists */
        dls = new ArrayList<>();
        pils = new ArrayList<>();
        plls = new ArrayList<>();
        /* Update game status to created */
        gameStatus = "Created";
    }

    /**
     * Create player object and add to game
     * @param name
     * @throws NoRoomForMorePlayersException
     */
    public void addPlayer(String name) throws NoRoomForMorePlayersException {
        if (players.size() < 4) {
            players.add(name);
            gameStatus = "Initiated";
        } else {
            throw new NoRoomForMorePlayersException();
        }
    }

    /**
     * Set player to inactive
     * @param name the players name
     */
    public void removePlayer(String name) {
        if (players.contains(name)) {
            players.set(players.indexOf(name), "Inactive: " + name);
            for (int i = 0; i < plls.size(); i++) {
                plls.get(i).playerStateChanged(new PlayerEvent(this, currentActivePlayer, PlayerEvent.LEFTGAME));
            }
        }
    }

    /**
     * Translate user position to board position
     * @param player the player of which the location should be returned
     * @param relativePiecePosition board position of player position (translatable)
     * @return absolute board position (16 - 91) relative to user position (0 - 59)
     */
    public int userGridToLudoBoardGrid(int player, int relativePiecePosition) {
        int boardPos = -1;
        /* position is start position, return current players start position */
        if (relativePiecePosition == 0) boardPos = 4 * player;
        /**
         * Each player is his/hers player number * 13 away from the other players starting positions
         * E.G: Red 1 = Green 14, Yellow = 27, Blue = 40. 0 - 15 is reserved for starting positions
         */
        else if (relativePiecePosition < 54) {
            boardPos = 15 + 13 * player + relativePiecePosition;
            if (boardPos > 67) {
                boardPos -= 52;
            }
            /* 68 through 92 is home stretch positions */
        } else boardPos = 68 + 6 * player;
        return boardPos;
    }

    /**
     * Checks how many active players there are.
     * @return the number of active players in the game
     */
    public int activePlayers() {
        int activePlayers = 0;
        for (String name : players) {
            if (!name.contains("Inactive:")) activePlayers++;
        }
        return activePlayers;
    }

    /**
     * Return number of players in the game (even inactive players)
     * @return total number of players
     */
    public int nrOfPlayers() {
        return players.size();
    }

    /**
     * returns the name of player with the correct player number.
     *
     * @param player_id the id of the player
     * @return either the name of the player or null
     */
    public String getPlayerName(int player_id) {
        if (players.size() > player_id)
            return players.get(player_id);
        else
            return null;
    }

    /**
     * Returns passed piece location
     * @param player RED, BLUE, YELLOW, GREEN
     * @param piece players piece
     * @return passed piece location of player on board
     */
    public int getPosition(int player, int piece) {
        return pieces[player][piece];
    }

    /**
     * Returns the currently active player
     * @return current active player
     */
    public int activePlayer() {
        return currentActivePlayer;
    }

    /**
     * User clicks throw dice button and returns generated value
     * @return value generated by <CLASS>
     */
    public int throwDice() {
        int face = random.nextInt(5) + 1;
        for (int i = 0; i < dls.size(); i++) {
            dls.get(i).diceThrown(new DiceEvent(this, activePlayer(), face));
        }
        if(face != 6) nextPlayer();
        return face;
    }

    /**
     *  Current player throws the dice
     * @param face dice face value
     * @return dice face value
     */
    public int throwDice(int face) {
        /* Becomes false if certain criteria are not met */
        nextPlayer = true;
        /* Game is not started and a player threw a six: update game status */
        if (gameStatus != "Started" && face == 6) gameStatus = "Started";
        /* Lucky player got a six */
        if (face == 6) {
            /* The player has pieces in play */
            if (pieceOnBoard()) numOfThrows++;
            nextPlayer = false;
            /* Player did not get a six */
        } else {
            /**
             * If the current player does not have any pieces in play and he/she has not thrown 3 times
             * it is still this players turn
             */
            if (!pieceOnBoard()) {
                numOfThrows++;
                nextPlayer = false;
            }
            if (numOfThrows == 2) {
                nextPlayer = false;
            }
        }
        for (int i = 0; i < dls.size(); i++) {
            dls.get(i).diceThrown(new DiceEvent(this, activePlayer(), face));
        }
        /* if nextPlayer is true, or the player has thrown 3 times: skip to next player */
        if (nextPlayer || numOfThrows == 3) nextPlayer();
        return face;
    }

    /**
     * Function that handles all logic related to moving a piece. Checks for cases such as movement being
     * blocked by opposing players tower and sending pieces back to home position if the players lands on
     * another players piece
     * @param player the player number to move
     * @param from player position to move from
     * @param to player position to move to
     * @return true in all cases
     */
    public boolean movePiece(int player, int from, int to) {
        /* The current piece to be moved */
        int piece = 0;
        /* Other player has piece at this location. 0 is reserved */
        int other = -1;

        /* Get global position of from/to move to check for other players */
        int fromGlobalPosition = userGridToLudoBoardGrid(player, from);
        int toGlobalPosition = userGridToLudoBoardGrid(player, to);
        /* Checks if there is another player at the to position */
        other = userGridToPlayerGrid[toGlobalPosition][0];
        /* Check if there is a tower of stacked pieces on the movement path */
        int towerPos = checkForTower(fromGlobalPosition + 1, fromGlobalPosition);
        int fromTowerPosition = findFreeSpotInTower(fromGlobalPosition);
        if (fromTowerPosition != 0) fromTowerPosition -= 1;
        if (towerPos != -1) {
            other = userGridToPlayerGrid[towerPos][0];
            /* Player is blocked by opposing stack */
            if (other != player) {
                toGlobalPosition = towerPos - 1;
            }
        }
        if (fromGlobalPosition != (68 + (player * 6) + 5)) {
            if (toGlobalPosition <= (68 + (player * 6) + 5)) {
                int toTowerPosition = findFreeSpotInTower(toGlobalPosition);
                /* Translate the user grid to player grid */
                userGridToPlayerGrid[fromGlobalPosition][fromTowerPosition] = -1;
                userGridToPlayerGrid[toGlobalPosition][toTowerPosition] = player;
                /* Which piece is decided by the location to move from */
                for (int i = 0; i < 4; i++) {
                    if (pieces[player][i] == from) {
                        /* Found piece at from location, assign it */
                        piece = i;
                        /* Move the piece */
                        pieces[player][i] = to;
                        break;
                    }
                }
                /* Update PieceListener */
                for (int i = 0; i < pils.size(); i++) {
                    pils.get(i).pieceMoved(new PieceEvent(this, player, piece ,from, to));
                }
                if (currentActivePlayer == player) {
                    /* Player moved out from home and it's now next players turn */
                    if (to == 1) {
                        nextPlayer();
                    } else if (numOfThrows == 2 && (to - from != 6)) {
                        nextPlayer();
                    }
                }
                /**
                 *  There is a piece located on the square the player is moving to
                 *  Place the other piece back at the owners start position
                 */
                if (other != player && other != -1) {
                    for (int i = 0; i < 4; i++) {
                        if (pieces[other][i] != 0) {
                            int gridPos = userGridToLudoBoardGrid(other, pieces[other][i]);
                            if (gridPos == toGlobalPosition) {
                                for (int j = 0; j < pils.size(); j++) {
                                    /* Update PieceListener */
                                    pils.get(i).pieceMoved(new PieceEvent(this, other, i ,pieces[other][i], 0));
                                }
                                pieces[other][i] = 0;
                                break;
                            }
                        }
                    } /* This section can be waaay more optimized, but unfortunately  */
                }     /* I did not have to refactor */
            }
        }
        /* Check for a winner after moving */
        checkForWinner();
        /**
         *  Could be the case that the player could not move because there
         *  are stacked pieces from another player blocking. However, we just omit updating
         *  the position and the move function always returns true even if the player didn't actually
         *  move a piece (maybe change this later)
         */
        return true;
    }

    /**
     * Checks to see if all players pieces is in goal. If so, we have a winner
     */
    private void checkForWinner() {
        /* Number of pieces in victory area */
        int winningPieces = 0;
        /* Loop on all players and pieces */
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                /* Player has a piece in goal, increment winningPieces */
                if (pieces[i][j] == 59)
                    winningPieces++;
            }
            if (winningPieces == 4) {
                gameStatus = "Finished";
                winner = i;
                /* There is a winner, update player events */
                for (int k = 0; k < plls.size(); k++) {
                    plls.get(i).playerStateChanged(new PlayerEvent(this, i, PlayerEvent.WON));
                }
                /* There is a winner, no need for more checks */
                break;
            }
        }
    }

    /**
     * Current status of the game
     * @return Initiated, Created, Started, Finished
     */
    public String getStatus() { return gameStatus; }

    /**
     * Try to get the winner. If there is no winner return -1
     * @return either the winner or -1
     */
    public int getWinner() { return (winner >= 0) ? winner : -1; }

    /* All the listeners */
    public void addDiceListener(DiceListener dl) { dls.add(dl); }
    public void addPieceListener(PieceListener pl) { pils.add(pl); }
    public void addPlayerListener(PlayerListener pl) { plls.add(pl); }

    /**
     * Check if the player has a piece on the board
     * @return true if there is a piece in play on the board, false else
     */
    private boolean pieceOnBoard() {
        for (int i = 0; i < 4; i++) {
            if (pieces[currentActivePlayer][i] != 0 && pieces[currentActivePlayer][i] != 59) {
                return true;
            }
        } return false;
    }

    /**
     * Checks if there are stacked pieces before moving
     * @param start from position
     * @param end to position
     * @return the int
     */
    private int checkForTower(int start, int end) {
        for (int i = start; i <= end; i++) {
            if (userGridToPlayerGrid[i][1] != -1) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Check for spot in the stacked pieces
     * @param gridPos the grid pos
     * @return the position of the stacked pieces
     */
    private int findFreeSpotInTower(int gridPos) {
        for (int i = 0; i < 4; i++) {
            if (userGridToPlayerGrid[gridPos][i] == -1) {
                return i;
            }
        } return 0;
    }

    /**
     * As long as the next player is not inactive, increase next player by one
     * resets to 0 if next player is above current number of players
     */
    private void nextPlayer() {
        /* Set last player to current player before updating current player */
        int lastPlayer = currentActivePlayer;
        /* You got here, reset number of throws */
        numOfThrows = 0;
        /* Increase the current player */
        currentActivePlayer = (1 + currentActivePlayer) % nrOfPlayers();
        /* If the next player is inactive, increase again */
        for(String playerName : players) {
            if(playerName.contains("Inactive")) {
                currentActivePlayer = (1 + currentActivePlayer) % nrOfPlayers();
            }
        }
        /* Update player states */
        for (int i = 0; i < plls.size(); i++) {
            plls.get(i).playerStateChanged(new PlayerEvent(this, lastPlayer, PlayerEvent.WAITING));
            plls.get(i).playerStateChanged(new PlayerEvent(this, currentActivePlayer, PlayerEvent.PLAYING));
        }
    }
}