package no.ntnu.imt3281.ludo.logic;

import java.util.EventObject;

/**
 * Event that handles the logic related to Pieces
 */
public class PieceEvent extends EventObject implements AutoCloseable {

	private int player, piece, boardStart, boardEnd;

	/**
	 *
	 * @param ludo The ludo object
	 * @param player RED, BLUE, YELLOW or GREEN player
	 * @param piece	The players piece
	 * @param boardStart Piece from location
	 * @param boardEnd Move to location
	 */
	public PieceEvent(Ludo ludo, int player, int piece, int boardStart, int boardEnd) {
		super(ludo);
		this.player = player;
		this.piece = piece;
		this.boardStart = boardStart;
		this.boardEnd = boardEnd;
	}

	/**
	 *
	 * @param obj The piece event object
	 * @return true if everything checks out, false if not
	 */
	@Override
	public boolean equals(Object obj) {
		try(PieceEvent pEvent = (PieceEvent) obj) {
			return (player == pEvent.player &&
					piece == pEvent.piece &&
					boardStart == pEvent.boardStart &&
					boardEnd == pEvent.boardEnd &&
					source == pEvent.source);
		} catch(Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
			return false;
		}
	}

	/**
	 * Assign the player
	 * @param player RED, BLUE, YELLOW, GREEN player
	 */
	public void setPlayer(int player) {
		this.player = player;
	}

	/**
	 * Retrieve the player
	 * @return RED, GREEN, YELLOW, BLUE
	 */
	public int getPlayer() {
		return player;
	}
	@Override
	public void close() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
