package no.ntnu.imt3281.ludo.client;

import no.ntnu.imt3281.ludo.Serializeable.Message;
import no.ntnu.imt3281.ludo.Serializeable.State;
import no.ntnu.imt3281.ludo.server.Config;

import java.io.*;
import java.net.Socket;

public class Connection {
    private final Socket s;
    private String name;
    public ObjectInputStream input;
    public ObjectOutputStream output;
    public BufferedReader bis;

    /**
     * Creates a new connection to the server
     * and keep hold on to the socket that communicates with the server
     * @throws IOException
     */
    public Connection() throws IOException {
        s = new Socket(Config.SERVERNAME, Config.SERVERPORT);
        bis = new BufferedReader(new InputStreamReader(s.getInputStream()));
        input = null;
        output = null;
    }

    /**
     * Send message to server
     * @param o Object to be send to server
     * @throws IOException if something goes wrong with ObjectOutputStream
     */
    public void send(Object o) throws IOException {
        try {
            if (output == null) {
                output = new ObjectOutputStream(s.getOutputStream());
            }
            output.writeObject(o);
            output.flush();
        } catch (IOException e) {
            System.out.println("Client failed to write");
        }
    }

    /**
     * Creates a new ObjectInputStream to communicate with the server if not already created
     * and read messages from the server when the socket contains data ready to be read
     * @return a generic object
     * @throws IOException if problems with ObjectInputStream
     * @throws ClassNotFoundException if class not found
     */
    public Object read()  throws IOException, ClassNotFoundException{
        if (input == null) {
            input = new ObjectInputStream(s.getInputStream());
        }
        if (bis.ready()) {
            return input.readObject();
        } else {
            return null;
        }
    }


    /**
     * Sends a message to the server notifying it about this client
     * leaving and then closes the connection.
     * @param userName
     */
    public void close(String userName) {
        try {
            output.writeObject(new Message(State.DISCONNECTED, userName));
            output.close();
            input.close();
            s.close();
        } catch(IOException e) {

        }
    }
}
