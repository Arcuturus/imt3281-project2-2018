package no.ntnu.imt3281.ludo.gui;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import no.ntnu.imt3281.ludo.Serializeable.Message;
import no.ntnu.imt3281.ludo.Serializeable.State;
import no.ntnu.imt3281.ludo.Serializeable.User;
import no.ntnu.imt3281.ludo.client.Connection;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.codec.digest.DigestUtils;

/*Controlls all of the fxml instances*/

public class LudoController {
	
	/*All main screen attachments*/
	@FXML private MenuItem random;
    @FXML private MenuItem connect;
	@FXML private TabPane tabbedPane;
	@FXML private PasswordField loginPassword;
	@FXML private TextField loginUsername;
	@FXML private TextField registerUsername;
	@FXML private Text loginError;
	@FXML private Text registerError;
	@FXML private PasswordField registerPassword;
	@FXML private PasswordField registerPassword2;
	@FXML private Button loginButton;
	@FXML private Button registerButton;
	@FXML private Tab LoginTab;
	

	/*Global chat and other variables*/
	@FXML private TextArea globalChatArea;
    @FXML private TextField globalTextToSay;
    @FXML private Button globalSendTextButton;
    @FXML private ListView<String> globalOnlineUsers;

    @FXML private ListView<String> globalFriendList;

    ExecutorService executor = Executors.newFixedThreadPool(1);


    private Connection connection;
    private String uid;

	// private LinkedBlockingQueue<Message> messages = new LinkedBlockingQueue<>();
    private boolean connected = false;
    private User user = null;
    private String userName;
    private ConcurrentLinkedQueue<String> queuePlayers;

    private ConcurrentHashMap<String, ConcurrentLinkedQueue<String>> acceptedChallenges;
    private final ObservableList<String> globalUserList = FXCollections.observableArrayList();
    private final ObservableList<String> trialQueue = FXCollections.observableArrayList();

    // Private controllers
    no.ntnu.imt3281.ludo.gui.challengeController challengeController;

    //private ExecutorService executor = Executors.newFixedThreadPool(1);

    /*public Connection getConnection() {
        return connection;
    }*/

    /*public LudoController()
    {
    	'
    }*/
    public Tab getLoginTab()
	{
		return LoginTab;
	}
    
    public TabPane getTabbedPane()
	{
		return tabbedPane;
	}
    
    public ObservableList<String> getGlobalUsers()
    {
    	return globalUserList;
    }
    
    public User getUser()
    {
    	return user;
    }

    Connection getConnection()
   	{
   		return connection;
   	}
    
    public String getUsername()
    {
    	return user.getUserName();
    }
    
    @FXML void initialize() {
        globalOnlineUsers.setItems(globalUserList);
    }
	
    /**
     * Registers a new user and inserts the user into a database.
     */
    @FXML public void registerUser()
	{
    	if(!registerPassword.getText().equals(registerPassword2.getText()) || !registerUsername.getText().isEmpty())
    	{
    		user = new User(registerUsername.getText(), DigestUtils.sha256Hex(registerPassword.getText()), true,registerUsername.getText());
    		connect();
    	}
    	else
    	{
    		registerError.setText("both password fields must be equal and username can not be empty");
    	}
    	if (connected) {
    		LoginTab.setDisable(true);
			//profile();
    	}
	}
    
    /**
     * Logs in the user.
     */
	@FXML public void loginUser()
	{       
		if(!loginUsername.getText().isEmpty() && !loginPassword.getText().isEmpty())
		{
			user = new User(loginUsername.getText(), DigestUtils.sha256Hex(loginPassword.getText()), false, loginUsername.getText());
			connect();
		}
		else
		{
			loginError.setText("Fill both of the login fields");
		}
	}
	
	@FXML
	public void joinRandomGame() {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("GameBoard.fxml"));
		loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
		
		
        // System.out.println(controller.ludo.getPlayerName(0));
		// Use controller to set up communication for this game.
		// Note, a new game tab would be created due to some communication from the server
		// This is here purely to illustrate how a layout is loaded and added to a tab pane.

		try {
			AnchorPane gameBoard = loader.load();
			Tab tab = new Tab("Game");
			tab.setContent(gameBoard);
			tabbedPane.getTabs().add(tab);
			GameBoardController controller = loader.getController();
			controller.setConnection(this.getConnection());
			//controller.setGameID(uid);
			controller.initiate();
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    }
	
	
	/**
	 * Lists all the available chatrooms and allows user to join or create a new chat.
	 */
    @FXML
    public void listRooms() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("Chat.fxml"));
        loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));

        try {
            AnchorPane chat = loader.load();
            Tab tab = new Tab("Chat");
            tab.setContent(chat);
            tabbedPane.getTabs().add(tab);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }
    
    /**
     * Opens a new tab with the user profile information.
     */
    @FXML
    public void profile() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("UserProfile.fxml"));
        loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));

        try {
            AnchorPane userProfile = loader.load();
            
            UserProfileController vg = loader.getController();
            vg.initiate(this);
            
            Tab tab = new Tab(loader.getResources().getString("userProfile.profile"));
            tab.setContent(userProfile);
            tab.setId("profileTab");
            tabbedPane.getTabs().add(tab);
            tabbedPane.getSelectionModel().select(tab);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

    }

    public void trialLoadList()
    {
    	//ActionEvent z = new ActionEvent();
    	//challengePlayers(z);
    	//for(int i = 0; i < globalUserList.size(); i++)
    		//System.out.println(globalUserList.get(i));
    	//trialOnlinePlayersList.setItems(globalUserList);
    	//System.out.println(user.getUserName());
    	/*if(trialQueue.size()<1)
		{
			//user.getUserName();
			trialQueue.add(user.getUserName());
			trialQueueList.setItems(trialQueue);
		}*/
    }

    /**
     * Opens a new tab where users can challenge each other to battles.
     */
    @FXML
    public void challengePlayers() {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("challengeController.fxml"));
        loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));

        try {
            AnchorPane challenge = loader.load();

            challengeController = loader.getController();
            challengeController.initiateList(this);
            Tab tab = new Tab(loader.getResources().getString("trial.challenge"));
            tab.setContent(challenge);
            tabbedPane.getTabs().add(tab);
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }        
        //trialOnlinePlayersList.setItems(globalOnlineUsers.getItems());
        
                
    }
    
    /**
     * Starts a private chat with another user from the friendlist.
     */
    @FXML
    public void startPrivateChat() {
    	String selectedPerson = globalOnlineUsers.getSelectionModel().getSelectedItem();
        System.out.println(selectedPerson);
    	if(selectedPerson == null)
    	{
    		Alert a = new Alert(AlertType.ERROR);
            a.setTitle("ERROR selecting person");
            a.setHeaderText("You have to select a person");
            a.setResizable(true);
            a.showAndWait();
    	} else {
    		//globalFriendList.getItems().add(selectedPerson);
    		FXMLLoader loader = new FXMLLoader(getClass().getResource("FriendChat.fxml"));
            loader.setResources(ResourceBundle.getBundle("no.ntnu.imt3281.I18N.i18n"));
            try {                
                Stage stage = new Stage();
                stage.setTitle(loader.getResources().getString("friendChat.window"));
                Parent root = (Parent)loader.load();
                stage.setScene(new Scene(root, 250, 350));
                stage.show();
                FriendChatController vg = loader.getController();
                vg.initiate(this);
                // Hide this current window (if this is what you want)
                //((Node)(event.getSource())).getScene().getWindow().hide();
            }
            catch (IOException e1) {
                e1.printStackTrace();
            }
    	}
    }

    /**
     * Adds another user as a friend. The request is send via sockets and then a popup window
     * is shown for the other user and he can choose to accept it or not.
     */
    @FXML
    public void addFriend()
    {
    	String selectedPerson = globalOnlineUsers.getSelectionModel().getSelectedItem();
    	if(selectedPerson == null) {
    		Alert a = new Alert(AlertType.ERROR);
            a.setTitle("ERROR selecting person");
            a.setHeaderText("You have to select a person");
            a.setResizable(true);
            a.showAndWait();
    	} else if (selectedPerson.equals(user.getUserName())) {
            System.out.println("You cant add yourself");
        } else {
            try {
                connection.send(new Message(State.ACK, user.getUserName(), selectedPerson, "FRIENDREQUEST"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    @FXML
    public void about(ActionEvent event) {
    }
    

    @FXML
    void trialStartGame(ActionEvent event) {

    }

    @FXML
    void sendFriendChatMessage(ActionEvent event) {

    }
    
    @FXML
    public void enterChatFromChatList(ActionEvent event) {
    }

    @FXML
    public void joinChatFromList(ActionEvent event) {
    }

    @FXML
    public void sendChatroomMessage(ActionEvent event) {
    }

    /**
     * If the client is already connected, disconnect the user
     * else establish a new connection with the server
     */
	@FXML
    public void connect() {
        try {
            if (connected) {
                connected = false;
                connect.setText("Connect");
                connection.send(new Message(State.DISCONNECTED, user.getUserName()));
                connection.close(user.getUserName());
                globalUserList.clear();
            } else {
                connection = new Connection();
                connected = true;
                connection.send(user);
                globalChatArea.setEditable(false);
                executor.execute(this::listen);
                connect.setText("Disconnect");
            }
        } catch (IOException e) {
            System.out.println("Failed in the connect function");
        }
    }

    /**
     * Creates a new thread listening for incoming messages from the server
     * and update the GUI based on the message information
     */
    private void listen() {
        while (connected) {
            try {
                if (connection.bis.ready()) {
                    Object o = connection.read();
                    if (o != null) {
                        Message msg = (Message) o;

                        Platform.runLater(() -> {
                            switch (msg.getState()) {
                                case JOINED:
                                    globalUserList.add(msg.getContent());
                                    break;
                                    
                                case LOGGEDIN:
                             		LoginTab.setDisable(true);
                            		profile();
                            		break;

                                case DISCONNECTED:
                                    globalUserList.remove(msg.getContent());
                                    break;

                                case GLOBALCHAT:
                                    globalChatArea.setText(globalChatArea.getText() + msg.getSender() + ": " + msg.getContent() + "\n");
                                    break;
                                    

                                /* Create a popup alert that a user wants to add you as a friend */
                                case FRIENDREQUEST:
                                    Alert alert = new Alert(AlertType.CONFIRMATION);
                                    alert.setTitle("Friend request");
                                    alert.setContentText(msg.getSender() + " has sent you a friend request!");
                                    Optional<ButtonType> result = alert.showAndWait();
                                    if (result.isPresent() && result.get() == ButtonType.OK) {
                                        globalFriendList.getItems().add(msg.getSender());
                                        try {
                                            connection.send(new Message(State.ACK, msg.getTarget(), msg.getSender(), "ACCEPTED"));
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    break;

                                case ACCEPTED:
                                    globalFriendList.getItems().add(msg.getSender());
                                    break;

                                /* When a user failed to log in */
                                case FAILED:
									try
									{
										connection.send(new Message(State.DISCONNECTED, getUsername()));
										connected = false;
										System.out.println("Login failed");
	                                	loginError.setText("Login failed");
	                                	connection.close(getUsername());
	                                	
									} catch (IOException e1)
									{
										// TODO Auto-generated catch block
										System.out.println(e1.getMessage());
										e1.printStackTrace();
									}
									break;
                                case CHANGEUSERNAME:
                                    //ObservableList<String> foo = globalOnlineUsers.getItems();
                                    //foo.remove(msg.getContent());
                                    break;


                                    /* Creates a new popup alerting the player that they have been challenged
                                     */
                                case CHALLENGE:
                                    Alert challenge = new Alert(AlertType.CONFIRMATION);
                                    challenge.setTitle("Game challenge");
                                    challenge.setContentText(msg.getSender() + " challenges you to a game!");
                                    Optional<ButtonType> challengeResult = challenge.showAndWait();
                                    if (challengeResult.isPresent() && challengeResult.get() == ButtonType.OK) {
                                        try {
                                            connection.send(new Message(State.ACCEPTCHALLENGE, msg.getTarget(), msg.getSender(), msg.getContent()));
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                    break;

                                    /* Update the challengeController when a user accepted their challenge */
                                case ACCEPTCHALLENGE:
                                    challengeController.getQueuedPlayers().add(msg.getSender());
                                    challengeController.getPendingUsersLV().getItems().add(msg.getSender());
                                    if(challengeController.getQueuedPlayers().size() == 2) {
                                        challengeController.startChallengeBtn.setDisable(false);
                                    }
                                    break;

                                case INITGAME:
                                    this.uid = msg.getContent();
                                    joinRandomGame();
                                    break;

                                default:
                                    System.out.println("Don't know where to broadcast this message");
                            }
                        });
                    }
                }
                Thread.sleep(50);
            } catch (InterruptedException | ClassNotFoundException | IOException e) {
                //e.printStackTrace();
                System.out.println("Failed in the listen function");
            }
        }
    }

    /**
     * Sends messages from the global chatting room to the server
     */
    @FXML
    public void sendGlobalMessage() {
        if (connected) {
            try {
                //System.out.println(globalTextToSay.getText());
                Message msg = new Message(State.GLOBALCHAT, user.getUserName(), globalTextToSay.getText());
                connection.send(msg);
                globalTextToSay.setText("");
            } catch (IOException e) {
                System.out.println("Could not send text!");
                connection.close(user.getUserName());
            }
        }
    }
}



