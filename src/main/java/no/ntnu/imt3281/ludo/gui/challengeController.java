package no.ntnu.imt3281.ludo.gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.Alert.AlertType;
import no.ntnu.imt3281.ludo.Serializeable.Message;
import no.ntnu.imt3281.ludo.Serializeable.State;
import no.ntnu.imt3281.ludo.client.Connection;

import java.io.IOException;
import java.util.UUID;

public class challengeController {

    @FXML private ListView<String> onlineUserList;
    @FXML private Button challengeInvite;
    @FXML private ListView<String> pendingUsersLV;
    @FXML Button startChallengeBtn;

    private Connection connection;
    private LudoController ludocontroller;
    private final ObservableList<String> queuedPlayers = FXCollections.observableArrayList();

    private String uuid;

    public challengeController()
    {
    	/* We get loadExceptionFXML if we try to initiate the list from the constructor
    	 * Thats why we have a separate method initiate() */
    	//onlineUserList.setItems(ludocontroller.getGlobalUsers());
    }
    
    /**
     * Initiate the list from which user can choose who to challenge to a fight
     * additionaly adds user to ready list.
     * @param ludocontroller lc
     */
    void initiateList(LudoController lc) {
        ludocontroller = lc;
        connection = ludocontroller.getConnection();
        ObservableList<String> trialQueueObsAvailable = lc.getGlobalUsers();
    	onlineUserList.setItems(trialQueueObsAvailable);
    	pendingUsersLV.getItems().add(lc.getUsername());
    	queuedPlayers.add(lc.getUsername());
    }


    /**
     * A client can challenge active online users to a game while less than 3 people has accepted the invitation
     * The request will create a UUID for the gamesession
     */
    @FXML
    void trialInvitePlayer() {
        String selectedPerson = onlineUserList.getSelectionModel().getSelectedItem();
        System.out.println("selectedPerson = " + selectedPerson);
    	if(selectedPerson == null || selectedPerson.equals(ludocontroller.getUsername())) {
    		Alert a = new Alert(AlertType.ERROR);
            a.setTitle("ERROR selecting person");
            a.setHeaderText("You have to select a person");
            a.setResizable(true);
            a.showAndWait();
    	} else {
    		if(queuedPlayers.size() < 4) {
                try {
                    uuid = UUID.randomUUID().toString();
    		        connection.send(new Message(State.SENDCHALLENGE, ludocontroller.getUsername(), selectedPerson, uuid.toString()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
    	}
    }


    /**
     * Notifies the server that the client would like to start the game
     */
    @FXML
     void startChallenge() {
        try {
            ludocontroller.getConnection().send(new Message(State.INITGAME, uuid));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * @return Scrollable view of users in the queue
     */
    ListView<String> getPendingUsersLV() {
        return pendingUsersLV;
    }
    
    /**
     * @return returns logic list of users in the queue
     */
    ObservableList<String> getQueuedPlayers() {
        return queuedPlayers;
    }


}
