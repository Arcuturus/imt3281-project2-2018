package no.ntnu.imt3281.ludo.gui;

import java.io.IOException;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import no.ntnu.imt3281.ludo.Serializeable.Message;
import no.ntnu.imt3281.ludo.Serializeable.State;

import org.apache.commons.codec.digest.DigestUtils;

public class UserProfileController {

    @FXML private Text profileDisplayUsername;
    @FXML private Button profileChangeUsernameButton;
    @FXML private Button profileChangePassword;
    @FXML private TextField profileChangeUsername;
    @FXML private PasswordField profileNewPassword1;
    @FXML private PasswordField profileNewPassword2;
    @FXML private Button logout;
    
    private LudoController ludocontroller;
    
    
    /**
     * Initiates the link between LudoController and UserProfileCOntroller,
     * as well as displays the current username.
     * @param lc
     */
    public void initiate(LudoController lc)
    {
    	ludocontroller = lc;
    	profileDisplayUsername.setText(ludocontroller.getUser().getUserName());
    }
    
    /**
     * Allows the user to change his current username.
     */
    @FXML
    public void changeUserName()
    {
    	if(!profileChangeUsername.getText().isEmpty())
    	{
    		String temp = ludocontroller.getUsername();
            try {
                ludocontroller.getConnection().send(new Message(State.CHANGEUSERNAME, profileChangeUsername.getText(), temp));
            } catch (IOException e) {
                e.printStackTrace();
            }
    	}
    	
    }
    
    /**
     * Allows the user to change his current password.
     */
    @FXML
    public void changeUserPassword()
    {
    	if(!profileNewPassword1.getText().isEmpty() || !profileNewPassword2.getText().isEmpty())
    	{
    		if(profileNewPassword1.getText().equals(profileNewPassword2.getText()))
    		{
    			String temp = ludocontroller.getUsername();
            	try {
            		ludocontroller.getConnection().send(new Message(State.CHANGEPASSWORD, temp,
            				DigestUtils.sha256Hex(profileNewPassword1.getText())));
            	} catch (IOException e) {
            		e.printStackTrace();
            	}
    		}
    		else
    		{
    			System.out.println("Password fields must be equal");
    		}
    	}
    	else
    	{
    		System.out.println("Both password fields must be filled");
    	}
    }
    
    /**
     * Logs out the user from the server.
     */
    @FXML
    public void logoutUser()
    {
    	ludocontroller.getLoginTab().setDisable(false);
        ludocontroller.getTabbedPane().getTabs().remove(ludocontroller.getTabbedPane().getSelectionModel().getSelectedItem());
    	ludocontroller.getConnection().close(ludocontroller.getUsername());
    }
}
