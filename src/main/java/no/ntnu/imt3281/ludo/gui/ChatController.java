package no.ntnu.imt3281.ludo.gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class ChatController {

    @FXML private ListView<?> joinChatroomList;
    @FXML private Button joinChatButton;
    @FXML private ListView<?> joinedChatroomList;
    @FXML private Button enterChatButton;
    @FXML private TextArea chatroomChatArea;
    @FXML private TextField chatroomTextToSay;
    @FXML private Button chatroomSendTextButton;
    @FXML private Text chatroomName;
    @FXML private TextField chatroomTextToSay1;
    @FXML private Button joinChatButton1;

    
    @FXML
    void enterChatFromChatList(ActionEvent event) {

    }

    @FXML
    void joinChatFromList(ActionEvent event) {

    }

    @FXML
    void sendChatroomMessage(ActionEvent event) {

    }

}
