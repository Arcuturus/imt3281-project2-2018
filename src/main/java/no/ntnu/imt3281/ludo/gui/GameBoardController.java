package no.ntnu.imt3281.ludo.gui;


import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

import no.ntnu.imt3281.ludo.client.Connection;
import no.ntnu.imt3281.ludo.logic.Ludo;

/**
 * Sample Skeleton for 'GameBoard.fxml' Controller Class
 */

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import no.ntnu.imt3281.ludo.client.Client;

import javax.swing.plaf.basic.BasicTreeUI;

public class GameBoardController {

    /* ==== FXML variables ==== */
    @FXML private Label player1Name;
    @FXML private ImageView player1Active;
    @FXML private Label player2Name;
    @FXML private ImageView player2Active;
    @FXML private Label player3Name;
    @FXML private ImageView player3Active;
    @FXML private Label player4Name;
    @FXML private ImageView player4Active;
    @FXML private ImageView diceThrown;
    @FXML private Button throwTheDice;
    @FXML private TextArea chatArea;
    @FXML private TextField textToSay;
    @FXML private Button sendTextButton;
    @FXML private AnchorPane root;

    /* ==== Other variables ==== */
    /* Holds all the dice images */
    private ArrayList<Image> dices;
    /* Board size in pixels */
    int boardSize = 722;
    /* Number of cells */
    int noOfCells = 15;
    /* Size of each square */
    private final double squareSize = (boardSize / noOfCells);
    /* Offset value for positioning the pieces at start locations */
    private final double cellOffset = (squareSize / 2);
    /* Client initialization */
    private Client client;
    /* Assign this game a unique id */
    private String uid;
    /* Player name */
    private String playerName;
    /* Player color */
    private int playerColor;
    /* Current active player (player turn) */
    private int activePlayer;
    /* The pieceColors */
    private ArrayList<Image> pieceColors;
    /*  */
    private ArrayList<ImageView> pieces;
    /* The board */
    private int[][] board;
    /* The client connection */
    private Connection con;
    /* The image for the piece */
    Image pieceImage;
    /* Ludo object to test locally */
    Ludo ludo;
    /* Player names */
    ArrayList<String> names;
    /*  */


    public GameBoardController() {
        //this.con = con;
        //this.uid = uid;
    	dices = new ArrayList<>();
    	pieceColors = new ArrayList<>();
    	names = new ArrayList<>();
        System.out.println("gameboard uid: " + uid);
    }
    
    @FXML
    /**
     * Animates the dice
     */
    public void animateDice() {
        /*TODO return(int) answer*/
        long t0 = System.currentTimeMillis();
        Timer t = new Timer();

        t.schedule(new TimerTask() {
            @Override
            public void run() {
                if (System.currentTimeMillis() - t0 > 3000)
                    cancel();
                Random rnd = new Random();
                int answer = rnd.nextInt(6);
                diceThrown.setImage(dices.get(answer));
            }
        }, 0, 100);
    }

//    /* Setters and getters related to the client */
//    private void setClient(Client client) {
//        this.client = client;
//    }
//
//    /* Not sure if you ever want to retrieve the client...? But what the heck */
//    public Client getClient() {
//        return this.client;
//    }

    public void setConnection(Connection con) {
        this.con = con;
    }

    /* Setters and getter related to GameID */
    /* Generate a unique */
    public void setGameID(String uid) {
        this.uid = uid;
    }

    public String getGameID() {
        return this.uid;
    }

    /* Setters and getters related to Player Names */
    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return this.playerName;
    }

    /* Setters and getters related to Player color */
    public void setPlayerColor(int color) {
        this.playerColor = color;

    }

    public int getPlayerColor() {
        return this.playerColor;
    }

    /* Setters and getters related to current active player */
    public void setActivePlayer(int activePlayer) {
        this.activePlayer = activePlayer;
    }

    public int getActivePlayer() {
        return this.activePlayer;
    }
    
    /* Assign player names and update fxml */
    public void setNames(ArrayList<String> names) {
        /* TODO check if name is null, then set player as empty seat */
        player1Name.setText(names.get(0));
        player2Name.setText(names.get(1));
        player3Name.setText(names.get(2));
        player4Name.setText(names.get(3));
    }

    protected void doSomeChatting(ActionEvent event) {
        /* socket stuff here */
        String message = textToSay.getText();
        /*
            Send message to/from client *
            Append message to message list (TextArea)
         */
    }


    /**
     * Initialize the different variables
     */
    public void initiate() {
        /* 2-dimensional array of the board */
        board = new int[][]{
                {-1,-1,-1,-1,-1,-1,65,66,67,-1,-1,-1,-1,-1,-1},
                {-1,-1,-1,-1,-1,-1,64,68,16,-1,-1,-1,-1,-1,-1},
                {-1,-1,-1,12,-1,-1,63,69,17,-1,-1, 0,-1,-1,-1},
                {-1,-1,13,-1,14,-1,62,70,18,-1, 2,-1, 1,-1,-1},
                {-1,-1,-1,15,-1,-1,61,71,19,-1,-1, 3,-1,-1,-1},
                {-1,-1,-1,-1,-1,-1,60,72,20,-1,-1,-1,-1,-1,-1},
                {54,55,56,57,58,59,-1,73,-1,21,22,23,24,25,26},
                {53,86,87,88,89,90,91,-1,79,78,77,76,75,74,27},
                {52,51,50,59,48,47,-1,85,-1,33,32,31,30,29,28},
                {-1,-1,-1,-1,-1,-1,46,84,34,-1,-1,-1,-1,-1,-1},
                {-1,-1,-1, 8,-1,-1,45,83,35,-1,-1, 4,-1,-1,-1},
                {-1,-1,9,-1, 11,-1,44,82,36,-1, 6,-1, 5,-1,-1},
                {-1,-1,-1,10,-1,-1,43,81,37,-1,-1, 7,-1,-1,-1},
                {-1,-1,-1,-1,-1,-1,42,80,38,-1,-1,-1,-1,-1,-1},
                {-1,-1,-1,-1,-1,-1,41,40,39,-1,-1,-1,-1,-1,-1}
        };
        /* Assign the dice face images to the list of dices */
        for(int i = 1; i <= 6; i++) {
    		Image img = new Image(getClass().getResource("../../../../../images/dice"+i+".png").toExternalForm());
    		dices.add(img);
    	}
        /* Dummy ludo object for local testing as the sockets does not currently work */
        ludo = new Ludo("Player1", "Player2", "Player3", "Player4");
        for (int i = 0; i < 4; i++) {
            if(ludo.getPlayerName(i) != null) {
                names.add(ludo.getPlayerName(i));
            }
        }
        /* Assign the names to the board labels */
        setNames(names);
        setup();
    }

    /**
     * Calculates where the piece is located on the board
     * @param piece Which piece to return the position for
     * @return return x,y position on board
     */
    public int[] getBoardPosition(int piece) {
        int[] coords = new int[2];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                if(board[i][j] == piece) {
                    coords[0] = i;
                    coords[1] = j;
                    return coords;
                }
            }
        }
        return null;
    }

    /**
     * Set pieces initial position on board
     */
    public void setup() {
        //con.send(null);
        String[] colors = {"r", "b", "y", "g"};
        int currentPiece = 0;
        for(int i = 0; i < 4; i++) {
            Image img = new Image(getClass().getResource("../../../../../images/ludo_piece_"+colors[i]+".png").toExternalForm());
            pieceColors.add(img);
        }
        pieces = new ArrayList<>();
        for(int i = 0; i < 4; i++) {
            for(int j = 0; j < 4; j++) {
                ImageView dummy = new ImageView(pieceColors.get(i));
                dummy.setFitWidth(squareSize);
                dummy.setFitHeight(squareSize);
                int[] location = getBoardPosition(currentPiece);
                switch(i) {
                    /* Red Player */
                    case 0: {
                        /* Calculations for piece 0 -3 */
                        dummy.setX(location[1]*squareSize + squareSize - cellOffset);
                        dummy.setY((location[0]*squareSize - cellOffset));
                    } break;
                    /* Blue Player */
                    case 1: {
                        /* Calculations for piece 4-7 */
                        dummy.setX(location[1]*squareSize + squareSize - cellOffset);
                        dummy.setY((location[0]*squareSize - cellOffset) + squareSize);
                    } break;
                    /* Yellow Player */
                    case 2: {
                        /* Calculations for piece 8-11 */
                        dummy.setX(location[1]*squareSize - cellOffset);
                        dummy.setY((location[0]*squareSize + squareSize - cellOffset));
                    } break;
                    /* Green Player */
                    case 3: {
                        /* Calculations for piece 12 - 15 */
                        dummy.setX(location[1]*squareSize - cellOffset);
                        dummy.setY((location[0]*squareSize - cellOffset));
                    } break;
                }
                /* assign so that we can bind a MouseEvent to the clicked piece */
                final int curPiece = currentPiece;
                root.getChildren().add(dummy);
                dummy.addEventHandler(MouseEvent.MOUSE_PRESSED, event ->  {
                    System.out.println(curPiece);
                    movePiece(dummy, curPiece);
                });
                currentPiece++;
            }
        }
    }

    /**
     * Supposed to be logic for moving the pieces. Never got this far really.
     * Should've been controlled by the Dice
     * @param iw The imageview for the player
     * @param pos from position
     */
    public void movePiece(ImageView iw, int pos) {
        int currentPlayer = pos/4;
        /* Piece is at start position */
        if(pos < 16) {
            if(currentPlayer == 0) {
                System.out.println("Player " + currentPlayer +  " wants to move out from location " + pos);
                int[] location = getBoardPosition(16);
                iw.setX(location[1]*squareSize);
                iw.setY(location[0]*squareSize);
            } else System.out.println("Not your turn");
        }
        /* Does not do anything */
        else {
            int next = pos;
            int[] location = getBoardPosition(pos+=4);
            iw.setX(location[1]*squareSize);
            iw.setY(location[0]*squareSize);
        }
    }

    public int userPositionToBoardPosition(int player, int piece) {
        return -1;
    }
}