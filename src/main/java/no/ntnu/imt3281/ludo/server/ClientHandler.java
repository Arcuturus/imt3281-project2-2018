package no.ntnu.imt3281.ludo.server;

import no.ntnu.imt3281.ludo.Serializeable.User;
import no.ntnu.imt3281.ludo.client.Client;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

public class ClientHandler {
    private final Socket s;
    private ObjectInputStream input;
    private ObjectOutputStream output;
    private boolean active = true;
    private BufferedReader br;
    private User user;

    private String name;

    private ConcurrentHashMap<String, Socket> gameSockets = new ConcurrentHashMap<>();



    /**
     * Creates a new handler for each client that connects to the server
     * @param s is the socket the client connected with
     * @throws IOException
     */
     ClientHandler(Socket s) throws IOException {
        this.s = s;
        input = new ObjectInputStream(s.getInputStream());
        output = null;
        br = new BufferedReader(new InputStreamReader(s.getInputStream()));
    }

    /**
     * Read incomming messages from the client
     * @return generic object that can be later cast to the right object
     */
     Object read(String condition) {
         try {
             if (condition.equals("Block")) {
                 return input.readObject();
             }
             if (br.ready())
                 return input.readObject();
             else
                 return null;
         } catch (IOException | ClassNotFoundException e) {
             e.printStackTrace();
             active = false;
             return null;
         }
     }

    /**
     * Write an object to the client
     * @param content the object to be sent to the client
     */
     void write(Object content) {
         try {
             if (output == null) {
                 output = new ObjectOutputStream(s.getOutputStream());
             }
             output.writeObject(content);
             output.flush();
         } catch (IOException e) {
             active = false;
         }
     }


    /**
     * @return the socket that the clienthandler uses to communicate with the client
     */
    Socket getSocket() {
        return s;
    }


    /**
     * Close the connection with the server
     */
    public void close() {
        try {
        	active = false;
            input.close();
            output.close();
            s.close();
        } catch (IOException e) {

        }
    }

    boolean isActive() {
        return active;
    }

    String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ConcurrentHashMap<String, Socket> getGameSockets() {
        return gameSockets;
    }

    public void setGameSockets(ConcurrentHashMap<String, Socket> gameSockets) {
        this.gameSockets = gameSockets;
    }
}
