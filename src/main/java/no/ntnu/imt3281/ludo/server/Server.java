package no.ntnu.imt3281.ludo.server;
import no.ntnu.imt3281.ludo.Serializeable.Message;
import no.ntnu.imt3281.ludo.Serializeable.State;
import no.ntnu.imt3281.ludo.Serializeable.User;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.ResultSet;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * This is the main class for the server.
 * **Note, change this to extend other classes if desired.**
 *
 * @author
 *
 */

/*
    TODO: Listen for request and send over necessary data
    TODO: Log chat
    TODO: Store every ongoing games and chat room information
 */
public class Server {
    private final Logger logger = Logger.getLogger(getClass().getName());
    private boolean shutdown = false;
   private DBController db;

    private final LinkedBlockingQueue<Message> messages = new LinkedBlockingQueue<>(); // List with messages
    private final ConcurrentHashMap<String, ClientHandler> clients = new ConcurrentHashMap<>(); // List of active clients
    private final ConcurrentHashMap<LudoHandler, ConcurrentLinkedQueue<ClientHandler>> activeGames = new ConcurrentHashMap<>();

    /**
     *  UUID for all game and keep track of every client in each unique game
     */
    private final ConcurrentHashMap<String, ConcurrentLinkedQueue<ClientHandler>> queuedPlayers = new ConcurrentHashMap<>();

    /**
     * Creates a cached thread pool
     * - that listens to incoming connections from clients
     * - that listens for new messages to send
     * - incoming messages from clients
     */
     private Server() {
         ExecutorService executor = Executors.newCachedThreadPool();
         executor.execute(this::connectionListenerThread);
         executor.execute(this::messageSenderThread);
         executor.execute(this::messageListenerThread);
         db = new DBController();
    }


    /**
     * Initialize the server
     */
    public static void main(String[] args) {
        new Server();
    }


    /**
     * Create a thread that listens on port SERVERPORT (specified in the config file)
     * and try to establish a connection with the client.
     * If it succeeds, add the client and store the socket communication.
     * System exits if failed to listen to SERVERPORT or failed to getting client connection
     */
    private void connectionListenerThread() {
        try (ServerSocket listener = new ServerSocket(Config.SERVERPORT)) {
            System.out.println("Server is listening on port " + Config.SERVERPORT);
            while (!shutdown) {
                Socket s = null;
                try {
                    s = listener.accept();
                    System.out.println("A new client is connected: " + s);
                } catch (IOException e) {
                    logger.log(Level.SEVERE, "Error while getting client connection: " + Config.SERVERPORT, e);
                    System.exit(0);
                }
                try {
                    addClient(s);
                } catch (IOException e) {
                    logger.log(Level.FINER, "Failed to establish connection with client", e);
                }
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Unable to listen to port" + Config.SERVERPORT, e);
            System.exit(0);
        }
    }


    /**
     * Listen for incoming messages from the clients sockets
     * and filter out the different requests from the users and add the messages to the queue containing all messages
     */

    private void messageListenerThread() {
        while (!shutdown) {
            clients.forEachValue(100, client -> {
                Object o = client.read("Dont block");
                if (o instanceof Message) {
                    Message msg = (Message) o;
                    switch (msg.getState()) {
                        /*
                         * Messages going to the global chat
                         */
                        case GLOBALCHAT:
                            messages.add(msg);
                            msgFromClient(client, msg);
                            break;

                        /*
                         * Acknowledgement message for friend request and challenge request
                         */
                        case ACK:
                            if(clients.containsKey(msg.getTarget())) {
                                messages.add(msg);
                            }
                            break;

                        /*
                         * Notify the server that the client is leaving
                         */
                        case DISCONNECTED:
                            if (clients.remove(msg.getContent()) != null) {
                                System.out.println(client.getName() + " is leaving now");
                                clientRemoved(client);
                            }
                            break;

                            /*
                            A user has sent a game challenge, and if the targeted user is online,
                            forward the request to the user
                             */

                        case SENDCHALLENGE:
                            ConcurrentLinkedQueue<ClientHandler> newPlayer = new ConcurrentLinkedQueue<>();
                            if (!queuedPlayers.containsKey(msg.getContent())) {
                                if (clients.containsKey(msg.getSender())) {
                                    newPlayer.add(clients.get(msg.getSender()));
                                }
                                queuedPlayers.put(msg.getContent(), newPlayer);
                            }
                            msg.setState(State.ACK);
                            messages.add(msg);
                            break;

                            /*
                            A user has accepted a challenge and sees if the player who challenged him is still online,
                            if that's the case, notify the user
                            */
                        case ACCEPTCHALLENGE:
                            if(queuedPlayers.containsKey(msg.getContent())) {
                                if (clients.containsKey(msg.getSender())) {
                                    System.out.println("Adding second: " + msg.getSender());
                                    queuedPlayers.get(msg.getContent()).add(clients.get(msg.getSender()));
                                }

                                ConcurrentLinkedQueue<ClientHandler> x = queuedPlayers.get(msg.getContent());
                            }
                            messages.add(msg);
                            break;


                            /*
                            Remove the users from a pending queue to a list containing all ongoing games
                             */
                        case INITGAME:
                            if (queuedPlayers.containsKey(msg.getContent())) {
                                messages.add(msg);
                            }
                            break;

                            /* A user request to change their username*/
                        case CHANGEUSERNAME:
                        	User user = client.getUser();
                            user.setUserName(msg.getSender());
                            user.setOldUserName(msg.getContent());

                        	if (!db.checkIfUserExist(user)) {
                        		db.updateUsername(user);
                        		client.setName(user.getUserName());
                        		messages.add(new Message(State.CHANGEUSERNAME, client.getName()));
                        	} else {
                        		messages.add(new Message(State.FAILED, client.getName(), user.getUserName()));
                        	}
                        	break;

                        	/* User request to change their password*/
                        case CHANGEPASSWORD:
                        	User user2 = client.getUser();
                            user2.setUserName(msg.getSender());
                            user2.setPassword(msg.getContent());
                        	db.updatePassword(user2);
                        	messages.add(new Message(State.CHANGEPASSWORD, client.getName()));
                        	break;
                        
                        case UPDATEGAMESPLAYED:
                        	User user3 = client.getUser();
                            user3.setUserName(msg.getSender());
                            db.updateMostPlayed(user3);
                        	messages.add(new Message(State.UPDATEGAMESPLAYED, client.getName()));
                        	break;

                        case UPDATEMOSTWINS:
                        	User user4 = client.getUser();
                            user4.setUserName(msg.getSender());
                            db.updateMostWins(user4);
                        	messages.add(new Message(State.UPDATEMOSTWINS, client.getName()));
                        	break;
                    }
                }
            });
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }


    /**
     * Watches the message queue and send messages to the clients
     */

    private void messageSenderThread() {
        while (!shutdown) {
            try {
                final Message msg = messages.take();
                switch (msg.getState()) {
                    /* The server will send an acknowledge to the user sending a request to the other user,
                    if he has responded
                     */
                    case ACK:
                        if (clients.containsKey(msg.getTarget())) {
                            if (msg.getContent().equals("FRIENDREQUEST")) {
                                msg.setState(State.FRIENDREQUEST);
                            } else if (msg.getContent().equals("ACCEPTED")) {
                                msg.setState(State.ACCEPTED);
                            } else {
                                msg.setState(State.CHALLENGE);
                                System.out.println(msg.getTarget());
                            }
                            clients.get(msg.getTarget()).write(msg);
                        }
                        break;
                        
                    case LOGGEDIN:
                    	clients.get(msg.getContent()).write(msg);
                    	break;

                        /* See if the user requesting a challenge is still online */
                    case ACCEPTCHALLENGE:
                        if (clients.containsKey(msg.getTarget())) {
                            clients.get(msg.getTarget()).write(msg);
                        }
                        break;

                        /* Default is broadcasting global chat*/
                        default:
                            clients.forEachValue(100, clientHandler -> clientHandler.write(msg));
                }
            } catch (InterruptedException e) {
                logger.log(Level.INFO, "Error fetching message from queue", e);
                Thread.currentThread().interrupt();
           }
            clients.forEachValue(100, clientHandler -> {
                if (!clientHandler.isActive() && clients.remove(clientHandler.getName()) != null) {
                    clientRemoved(clientHandler);
                }
            });
        }
    }


    /**
     * The server will check if the user that tried to connect already exist or registered with a username that's
     * already in use. It will then respond to the user with appropriate
     * @param s is the socket the ClientHandler will communicate with the client
     * @throws IOException
     */
    private void addClient(Socket s) throws IOException {
        final ClientHandler client = new ClientHandler(s);
        Object o = client.read("Block");
        if (o != null) {
            User user = (User) o;
            client.setUser(user);
            System.out.println(client.getName());
            /* The user logs in and the server will check with the database to see if the user exist */
            if(!user.checkNewUser() && db.checkIfUserExist(user))
            {
            	client.setName(user.getUserName());
            	clients.forEachKey(100, name -> client.write(new Message(State.JOINED, name)));
            	clients.put(client.getName(), client);
            	messages.add(new Message(State.LOGGEDIN, client.getName()));
            	messages.add(new Message(State.JOINED, client.getName()));
            }
            /* else, if the user register a new user, check if the username already exist */
            else if(user.checkNewUser() && !db.checkIfUserExist(user))
            {
            	System.out.println("neida, du er her");
            	client.setName(user.getUserName());
            	db.insertUser(user);
            	clients.forEachKey(100, name -> client.write(new Message(State.JOINED, name)));
            	clients.put(client.getName(), client);
            	messages.add(new Message(State.LOGGEDIN, client.getName()));
            	messages.add(new Message(State.JOINED, client.getName()));
            }
            /* If login information is wrong or username already is registered */
            else {
            	client.write(new Message(State.FAILED, client.getUser().getUserName()));
            }
        } else {
            System.out.println("Is null");
        }
    }

    /**
     * Logs the messages in the global chat to the log.txt file
     * @param client the client who sent the message
     * @param msg  the content of the message the user sent
     */
    private void msgFromClient(ClientHandler client, Message msg)
    {
        try (FileWriter fw = new FileWriter("log.txt", true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw))
        {
            String message = msg.getContent();
            out.write(client.getName() + " said: " + message + "\n");
            out.flush();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * Close the connection with the client and add a DISCONNECTED message to the messages list
     * @param client
     */
    private void clientRemoved(ClientHandler client) {
        client.close();
        messages.add(new Message(State.DISCONNECTED, client.getName()));
    }
}
