package no.ntnu.imt3281.ludo.server;
import no.ntnu.imt3281.ludo.Serializeable.GameState;
import no.ntnu.imt3281.ludo.Serializeable.Message;
import no.ntnu.imt3281.ludo.logic.Ludo;

import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Handles all the ludo logic related to the server and clients
 */
public class LudoHandler {

    /* Get the names of the players from the server */
    /* Create Ludo class with the names from the server */
    /* Broadcast all required information to each player */
    /* Start the game handler logic */

    private String UUID;
    private Ludo ludo;
    private boolean finished = false;
    ConcurrentLinkedQueue<ClientHandler> clients;
    private Random random;

    /**
     * LudoHandler constructor
     * @param UUID the unique id of the game
     * @param players all the participating players
     */
    public LudoHandler(String UUID, ConcurrentLinkedQueue<ClientHandler> players) {
        ludo = new Ludo();
        this.clients = players;
        this.UUID = UUID;
        this.random = new Random();
        for(ClientHandler player : players) {
            System.out.println("Added: " + player.getName() + " to Ludo (" + UUID + ")");
            ludo.addPlayer(player.getName());
        }
        StartGame(ludo);
    }

    /**
     * This is where we planned to have the "main" ludo master controller.
     * All game related events were supposed to be handled here and broadcasted to all the
     * players in the clients (players list).
     * @param ludo The ludo object
     */
    public void StartGame(Ludo ludo) {
        /* Run as long as the game has not ended */
        if(!finished) {
            for(ClientHandler client : clients) {
                /* Broadcast active player to board */
                client.write(new Message(GameState.ACTIVEPLAYER, ludo.activePlayer()));
                int face = random.nextInt(5) + 1;
            }
        }
    }
}
