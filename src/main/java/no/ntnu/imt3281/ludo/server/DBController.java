package no.ntnu.imt3281.ludo.server;

import java.sql.Connection;  
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import no.ntnu.imt3281.ludo.Serializeable.User;


public class DBController
{
	/**
	 * Constructor test if the connection can be established, hence the 'temp' variable name
	 */
	public DBController()
	{
		try
    	{
			Connection temp = DriverManager.getConnection("jdbc:mysql://localhost:3306/ludodb", "root", "");
    		System.out.println("Connection Successful");
    	}
    	catch (SQLException e)
    	{
    		System.out.println("error: "+e.getMessage());
		}
	}

	/**
	 * Inserting user into a database. User is inserted in different tables, user table
	 * mostwin table and most played table.
	 * @param User which will be inserted into the database
	 */
	public void insertUser(User user)
	{
    	try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ludodb", "root", ""))
    	{
    		/*Insert user into user table*/
    		String query = "INSERT INTO user ("+"id,"+" name,"+"pass) VALUES (null, ?, ?)";
    		PreparedStatement st = connection.prepareStatement(query);
    		st.setString(1, user.getUserName());
    		st.setString(2, user.getPassword());
    		st.executeUpdate();
    		
    		/*get userID from user table*/
    		String query2 = "SELECT id FROM user WHERE name=?";
    		PreparedStatement st2 = connection.prepareStatement(query2);
    		st2.setString(1, user.getUserName());
    		ResultSet rs = st2.executeQuery();
    		rs.next();
    		
    		/*Insert user into Mostwins table*/
    		String query3 = "INSERT INTO mostwins ("+"id,"+" userId,"+"numberOfWins) VALUES (null, ?, ?)";
    		PreparedStatement st3 = connection.prepareStatement(query3);
    		st3.setInt(1, rs.getInt(1));
    		st3.setInt(2, 0);
    		st3.executeUpdate();
    		
    		/*Insert user into Mostplayed table*/
    		String query4 = "INSERT INTO mostplayed ("+"id,"+" userId,"+"numberOfGames) VALUES (null, ?, ?)";
    		PreparedStatement st4 = connection.prepareStatement(query4);
    		st4.setInt(1, rs.getInt(1));
    		st4.setInt(2, 0);
    		st4.executeUpdate();
    		
    		System.out.println("USER ADDED SUCCESSFULLY");
    	} 
    	catch (SQLException e)
    	{
    		System.out.println("user could not be added "+e.getMessage());
		}
    }
	
	/**
	 * Functions check if username already exist in the database.
	 * We do not allow duplicates in the database.
	 * @param Takes a User as a parameter
	 * @return returns True if user exists in database
	 */
	public boolean checkIfUserExist(User user)
	{
		try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ludodb", "root", ""))
    	{
    		/*Check if username exists in the database*/
    		String query = "SELECT name FROM user WHERE name=?";
    		PreparedStatement st = connection.prepareStatement(query);
    		st.setString(1, user.getUserName());
    		ResultSet rs = st.executeQuery();
    		rs.next();
    		
    		return (user.getUserName().equals(rs.getString(1)));
    	} 
    	catch (SQLException e)
    	{
    		System.out.println("user could not be found "+e.getMessage());
    		return false;
		}
	}
	
	/**
	 * Updates users username
	 * @param User who wants to have his name changed
	 */
	public void updateUsername(User user)
	{
		try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ludodb", "root", ""))
    	{
			/*get userID from user table*/
    		String query1 = "SELECT id FROM user WHERE name=?";
    		PreparedStatement st1 = connection.prepareStatement(query1);
    		st1.setString(1, user.getOldUserName());
    		ResultSet rs1 = st1.executeQuery();
    		rs1.next();
    		System.out.println("Old22: " + user.getOldUserName() + "  New22: " + user.getUserName());
    		
    		/*update username for user with given id*/
    		String query2 = "UPDATE user SET name = ? WHERE id = ?";
    		PreparedStatement st2 = connection.prepareStatement(query2);
    		st2.setString(1, user.getUserName());
    		st2.setInt(2, rs1.getInt(1));
    		System.out.println(rs1.getInt(1));
    		st2.executeUpdate();
    	} 
    	catch (SQLException e)
    	{
    		System.out.println("user could not be found "+e.getMessage());
		}
	}
	
	/**
	 * Updates users password
	 * @param User who wants to have his password updated
	 */
	public void updatePassword(User user)
	{
		try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ludodb", "root", ""))
    	{
			/*get userID from user table*/
    		String query = "SELECT id FROM user WHERE name=?";
    		PreparedStatement st = connection.prepareStatement(query);
    		st.setString(1, user.getUserName());
    		ResultSet rs = st.executeQuery();
    		rs.next();
    		
			/*update password for user with given id*/
    		String query2 = "UPDATE user SET pass = ? WHERE id = ?";
    		PreparedStatement st2 = connection.prepareStatement(query2);
    		st2.setString(1, user.getPassword());
    		st2.setInt(2, rs.getInt(1));
    		st2.executeUpdate();
    		
    	} 
    	catch (SQLException e)
    	{
    		System.out.println("Connection could not be established "+e.getMessage());
		}
	}
	
	/**
	 * Increments the mostWin column for given User
	 * @param User for which the mostWin shall be incremented
	 */
	public void updateMostWins(User user)
	{
		try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ludodb", "root", ""))
    	{
			/*get userID from user table*/
    		String query1 = "SELECT id FROM user WHERE name=?";
    		PreparedStatement st1 = connection.prepareStatement(query1);
    		st1.setString(1, user.getUserName());
    		ResultSet rs1 = st1.executeQuery();
    		rs1.next();
    		
    		/*get number of wins from user in order to increase it later*/
    		String query2 = "SELECT numberOfWins FROM mostwins WHERE userId=?";
    		PreparedStatement st2 = connection.prepareStatement(query2);
    		st2.setInt(1, rs1.getInt(1));
    		ResultSet rs2 = st2.executeQuery();
    		rs2.next();
    		
			/*update number of wins*/
    		String query3 = "UPDATE mostwins SET numberOfWins = ? WHERE userId = ?";
    		PreparedStatement st3 = connection.prepareStatement(query3);
    		st3.setInt(1, (rs2.getInt(1)+1));
    		st3.setInt(2, rs1.getInt(1));
    		st3.executeUpdate();
    		
    	} 
    	catch (SQLException e)
    	{
    		System.out.println("Connection could not be established "+e.getMessage());
		}
	}
	
	/**
	 * Increments the mostPlayed column for given User
	 * @param User for which the mostPlayed shall be incremented
	 */
	public void updateMostPlayed(User user)
	{
		try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ludodb", "root", ""))
    	{
			/*get userID from user table*/
    		String query1 = "SELECT id FROM user WHERE name=?";
    		PreparedStatement st1 = connection.prepareStatement(query1);
    		st1.setString(1, user.getUserName());
    		ResultSet rs1 = st1.executeQuery();
    		rs1.next();
    		
    		/*get number of games played from user in order to increase it later*/
    		String query2 = "SELECT numberOfGames FROM mostplayed WHERE userId=?";
    		PreparedStatement st2 = connection.prepareStatement(query2);
    		st2.setInt(1, rs1.getInt(1));
    		ResultSet rs2 = st2.executeQuery();
    		rs2.next();
    		
			/*update number of games played*/
    		String query3 = "UPDATE mostplayed SET numberOfGames = ? WHERE userId = ?";
    		PreparedStatement st3 = connection.prepareStatement(query3);
    		st3.setInt(1, (rs2.getInt(1)+1));
    		st3.setInt(2, rs1.getInt(1));
    		st3.executeUpdate();
    		
    	} 
    	catch (SQLException e)
    	{
    		System.out.println("Connection could not be established "+e.getMessage());
		}
	}
	
	/**
	 * Method combines two tables, userId and mostWins columns
	 * @return Returns combined result from two tables
	 */
	public ResultSet getTopTenWins()
	{
		try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ludodb", "root", ""))
    	{
			String query1 = "SELECT mostwins.numberOfWins, user.name "
					+ "FROM user LEFT JOIN mostwins ON mostwins.userId = user.id "
					+ "ORDER BY numberOfWins DESC";
    		PreparedStatement st1 = connection.prepareStatement(query1);    		
    		return st1.executeQuery();
    	} 
    	catch (SQLException e)
    	{
    		System.out.println("Connection could not be established "+e.getMessage());
    		return null;
		}
	}
	
	/**
	 * Method combines two tables, userId and mostPlayed columns
	 * @return Returns combined result from two tables
	 */
	public ResultSet getTopTenPlayed()
	{
		try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ludodb", "root", ""))
    	{
			String query1 = "SELECT mostplayed.numberOfGames, user.name "
					+ "FROM user LEFT JOIN mostplayed ON mostplayed.userId = user.id "
					+ "ORDER BY numberOfGames DESC";
    		PreparedStatement st1 = connection.prepareStatement(query1);    		
    		return st1.executeQuery();
    	} 
    	catch (SQLException e)
    	{
    		System.out.println("Connection could not be established "+e.getMessage());
    		return null;
		}
	}
	
	/**
	 *  Method adds two users as friends
	 * @param First user
	 * @param Second user
	 */
	public void addFriend(User user1, User user2)
	{
		try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ludodb", "root", ""))
    	{
			/*get userID from user table, first user*/
    		String query1 = "SELECT id FROM user WHERE name=?";
    		PreparedStatement st1 = connection.prepareStatement(query1);
    		st1.setString(1, user1.getUserName());
    		ResultSet rs1 = st1.executeQuery();
    		rs1.next();
    		
    		/*get userID from user table, second user*/
    		String query2 = "SELECT id FROM user WHERE name=?";
    		PreparedStatement st2 = connection.prepareStatement(query2);
    		st2.setString(1, user2.getUserName());
    		ResultSet rs2 = st2.executeQuery();
    		rs2.next();
    		
    		/*Insert user into friendship table*/
    		String query3 = "INSERT INTO friendship ("+"id,"+" userId1,"+"userId2) VALUES (null, ?, ?)";
    		PreparedStatement st3 = connection.prepareStatement(query3);
    		st3.setInt(1, rs1.getInt(1));
    		st3.setInt(2, rs2.getInt(1));
    		st3.executeUpdate();
    		
    		/*Insert user into friendship table again but in reversed order, the reasoning:
    		https://www.quora.com/How-does-Facebook-maintain-a-list-of-friends-for-each-user-Does-it-maintain-a-separate-table-for-each-user*/
    		String query4 = "INSERT INTO friendship ("+"id,"+" userId1,"+"userId2) VALUES (null, ?, ?)";
    		PreparedStatement st4 = connection.prepareStatement(query4);
    		st4.setInt(1, rs2.getInt(1));
    		st4.setInt(2, rs1.getInt(1));
    		st4.executeUpdate();
    	} 
    	catch (SQLException e)
    	{
    		System.out.println("Connection could not be established "+e.getMessage());
		}
	}
	
	
	/**
	 * NOT FINISHED, should return a list of friends to a given user
	 * @param user which we want to get friends from
	 */
	public void getFriends(User user)
	{
		try(Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/ludodb", "root", ""))
    	{
			String query1 = "SELECT user.name, friendship.userId2 "
					+ "FROM user LEFT JOIN friendship ON friendship.userId2 = user.name";
			PreparedStatement st1 = connection.prepareStatement(query1);
    	} 
    	catch (SQLException e)
    	{
    		System.out.println("Connection could not be established "+e.getMessage());
		}
	}
	
}
