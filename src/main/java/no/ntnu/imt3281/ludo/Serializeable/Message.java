package no.ntnu.imt3281.ludo.Serializeable;

import java.io.Serializable;

public class Message implements Serializable {

    private State state;
    private GameState gState;
    private String content;
    private String sender;
    private String target;
    private int activePlayer;

    
    /**
     * Used when you only need to send some content, e.g. global chat
     * @param state makes the server and client know what action to do
     * @param content, the content of the message
     */
    public Message(State state, String content) {
        this.state = state;
        this.content = content;
    }

    /**
     * Send different states in a game
     * @param gState game state
     * @param activePlayer the active player
     */
    public Message(GameState gState, int activePlayer) {
        this.gState = gState;
        this.activePlayer = activePlayer;
    }

    /**
     * 
     * @param state, makes the server and client know what action to do
     * @param sender the sender of the message
     * @param content the content of the message
     */
    public Message(State state, String sender, String content) {
        this.state = state;
        this.sender = sender;
        this.content = content;
    }

    /**
     * 
     * @param state, state makes the server and client know what action to do
     * @param sender the sender of the message
     * @param target the target of the message
     * @param content the message content
     */
    public Message(State state, String sender, String target, String content) {
        this.state = state;
        this.sender = sender;
        this.target = target;
        this.content = content;
    }

    /**
     * @param state: set message state
     */
    public void setState(State state) {this.state = state;}

    /**
     * 
     * @return message state
     */
    public State getState() {
        return this.state;
    }

    /**
     * 
     * @return content in the message
     */
    public String getContent() {
        return content;
    }

    /**
     * Get who sent the message
     * @return sender
     */
    public String getSender() { return sender; }

    /**
     * Get who the message was to
     * @return player the message was meant to
     */
    public String getTarget() {
        return target;
    }
}
