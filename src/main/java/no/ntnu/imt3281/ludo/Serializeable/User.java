package no.ntnu.imt3281.ludo.Serializeable;

import java.io.Serializable;

public class User implements Serializable {
    private String userName;
    private String password;
    private boolean newUser;
    private String oldUserName;

	public User(String name, String pw, boolean flag, String oldName) {
        this.userName = name;
        this.password = pw;
        this.newUser = flag;
        this.oldUserName = oldName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        System.out.println("User class new: " + userName);
        this.userName = userName;
    }
    
    public String getOldUserName()
   	{
   		return oldUserName;
   	}

   	public void setOldUserName(String oldUserName)
   	{
        System.out.println("User class old: " + oldUserName);
   		this.oldUserName = oldUserName;
   	}
    
    public boolean checkNewUser() {
    	return newUser;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
