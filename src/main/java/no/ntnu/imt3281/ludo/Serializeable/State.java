package no.ntnu.imt3281.ludo.Serializeable;

/**
 * The available message states the client and server can communicate with each other
 */
public enum State {
    JOINED, DISCONNECTED, GLOBALCHAT, FRIENDREQUEST, CHALLENGE, ACCEPTED, ACK, CHANGEUSERNAME, FAILED, SENDCHALLENGE,
    ACCEPTCHALLENGE, CHANGEPASSWORD, UPDATEMOSTWINS, UPDATEGAMESPLAYED, INITGAME, LOGGEDIN
}
