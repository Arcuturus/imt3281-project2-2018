package no.ntnu.imt3281.ludo.Serializeable;

import java.io.Serializable;

public enum GameState implements Serializable {
    ACTIVEPLAYER,
    THROWDICE,
    UPDATEBOARD
}
